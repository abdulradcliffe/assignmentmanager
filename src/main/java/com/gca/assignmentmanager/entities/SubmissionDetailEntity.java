package com.gca.assignmentmanager.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "submission_dtl")
public class SubmissionDetailEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "submission_id")
    private Long submissionId;

    @Column(name = "question_id")
    private Long questionId;

    @Column(name = "given_answer_text")
    private String answerText;

}
