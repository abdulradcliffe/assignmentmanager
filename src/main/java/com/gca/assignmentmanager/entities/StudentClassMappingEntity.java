package com.gca.assignmentmanager.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "student_class_mapping")
public class StudentClassMappingEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "student_id")
    private Long studentId;

    @Column(name = "class_id")
    private Long classId;

}
