package com.gca.assignmentmanager.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "teacher_class_subject_mapping")
public class TeacherClassSubjectmappingEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "teacher_id")
    private Long teacherId;

    @Column(name = "class_id")
    private Long classId;

    @Column(name = "subject_id")
    private Long subjectId;

}
