package com.gca.assignmentmanager.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "submission_mst")
public class SubmissionMasterEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "assignment_id")
    private Long assignmentId;

    @Column(name = "submitted_by")
    private Long submittedBy;

    @Column(name = "created_on")
    private Date createdOn;

}
