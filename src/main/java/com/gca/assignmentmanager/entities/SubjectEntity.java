package com.gca.assignmentmanager.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "subject_mst")
public class SubjectEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "subject_name")
    private String subjectName;

}
