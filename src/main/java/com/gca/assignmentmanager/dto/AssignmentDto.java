package com.gca.assignmentmanager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssignmentDto {

    private Long id;
    private String assignmentName;
    private String topic;
    private Long teacherId;
    private Long classId;
    private Long subjectId;
    private Date createdOn;
    private List<AssignmentQuestionDto> questions;

}
