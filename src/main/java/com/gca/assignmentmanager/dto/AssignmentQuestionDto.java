package com.gca.assignmentmanager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssignmentQuestionDto {

    private Long id;
    private Long assignmentId;
    private String questionText;

}
