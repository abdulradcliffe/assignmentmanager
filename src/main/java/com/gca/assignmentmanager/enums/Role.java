package com.gca.assignmentmanager.enums;

public enum Role {

    TEACHER("TEACHER"), STUDENT("STUDENT");

    String role;

    Role(String role) {
        this.role = role;
    }
}
