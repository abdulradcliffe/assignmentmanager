package com.gca.assignmentmanager.repo;

import com.gca.assignmentmanager.entities.ClassEntity;
import com.gca.assignmentmanager.entities.TeacherClassSubjectmappingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherClassSubjectMappingRepository extends JpaRepository<TeacherClassSubjectmappingEntity, Long> {
    List<TeacherClassSubjectmappingEntity> findByTeacherId(Long teacherId);
}
