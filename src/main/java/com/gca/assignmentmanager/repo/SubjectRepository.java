package com.gca.assignmentmanager.repo;

import com.gca.assignmentmanager.entities.SubjectEntity;
import com.gca.assignmentmanager.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<SubjectEntity, Long> {
    List<SubjectEntity> findByIdIn(List<Long> subjectIds);
}
