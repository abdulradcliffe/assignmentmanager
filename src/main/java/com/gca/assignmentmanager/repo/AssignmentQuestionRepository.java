package com.gca.assignmentmanager.repo;

import com.gca.assignmentmanager.entities.AssignmentEntity;
import com.gca.assignmentmanager.entities.AssignmentQuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssignmentQuestionRepository extends JpaRepository<AssignmentQuestionEntity, Long> {
    List<AssignmentQuestionEntity> findByAssignmentId(Long id);

    void deleteByAssignmentId(Long id);
}
