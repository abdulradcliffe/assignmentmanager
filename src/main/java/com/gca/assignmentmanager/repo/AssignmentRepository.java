package com.gca.assignmentmanager.repo;

import com.gca.assignmentmanager.entities.AssignmentEntity;
import com.gca.assignmentmanager.entities.ClassEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssignmentRepository extends JpaRepository<AssignmentEntity, Long> {
    List<AssignmentEntity> findByTeacherId(Long teacherId);

    List<AssignmentEntity> findByTeacherIdAndClassId(Long teacherId, Long classId);

    List<AssignmentEntity> findByTeacherIdAndSubjectId(Long teacherId, Long subjectId);

    List<AssignmentEntity> findByTeacherIdAndClassIdAndSubjectId(Long teacherId,Long classId, Long subjectId);

    List<AssignmentEntity> findByClassId(Long classId);

    List<AssignmentEntity> findByIdIn(List<Long> assignmentIds);
}
