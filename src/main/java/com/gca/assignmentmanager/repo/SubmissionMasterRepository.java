package com.gca.assignmentmanager.repo;

import com.gca.assignmentmanager.entities.SubmissionDetailEntity;
import com.gca.assignmentmanager.entities.SubmissionMasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubmissionMasterRepository extends JpaRepository<SubmissionMasterEntity, Long> {

    List<SubmissionMasterEntity> findByAssignmentId(Long assignmentId);

    SubmissionMasterEntity findByAssignmentIdAndSubmittedBy(Long id, Long studentId);

    List<SubmissionMasterEntity> findBySubmittedBy(Long studentId);
}
