package com.gca.assignmentmanager.repo;

import com.gca.assignmentmanager.entities.ClassEntity;
import com.gca.assignmentmanager.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassRepository extends JpaRepository<ClassEntity, Long> {
}
