package com.gca.assignmentmanager.repo;

import com.gca.assignmentmanager.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByEmailAndPassword(String email, String password);

    UserEntity findByEmail(String email);
}
