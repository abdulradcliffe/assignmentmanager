package com.gca.assignmentmanager.repo;

import com.gca.assignmentmanager.entities.ClassEntity;
import com.gca.assignmentmanager.entities.StudentClassMappingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentClassMappingRepository extends JpaRepository<StudentClassMappingEntity, Long> {

    StudentClassMappingEntity findByStudentId(Long studentId);
}
