package com.gca.assignmentmanager.repo;

import com.gca.assignmentmanager.entities.ClassEntity;
import com.gca.assignmentmanager.entities.SubmissionDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubmissionDetailRepository extends JpaRepository<SubmissionDetailEntity, Long> {

    List<SubmissionDetailEntity> findBySubmissionId(Long submissionId);
}
