package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gca.assignmentmanager.entities.UserEntity;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LoginAPIResponse {

    private boolean status;
    private String message;
    private LoginAPIResponseUserDetails userDetail;
}
