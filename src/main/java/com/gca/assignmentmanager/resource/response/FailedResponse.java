package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gca.assignmentmanager.enums.Constants;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class FailedResponse {

    private String status = Constants.FAILED;
    private String message;
}
