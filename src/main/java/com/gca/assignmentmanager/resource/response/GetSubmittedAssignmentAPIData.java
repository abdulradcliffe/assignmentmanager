package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetSubmittedAssignmentAPIData {
    private Long assignmentId;
    private Long submissionId;
    private String teacherName;
    private String subject;
    private Date createdOn;
    private String topicName;
    private Date submissionDate;
}
