package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gca.assignmentmanager.enums.Role;
import lombok.Data;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto {

    private Long id;
    private String name;
    private String email;
    private Date dob;
    private Role role;
}
