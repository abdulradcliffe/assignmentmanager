package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gca.assignmentmanager.dto.AssignmentDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetPendingAssignmentAPIResponse {
    private String status;
    private String message;
    private List<GetPendingAssignmentAPIData> data;
}
