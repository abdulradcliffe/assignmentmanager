package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubmissionMstDto {

    private Long submissionId;
    private Long submittedById;
    private String submittedByName;
    private Date submissionDate;
}
