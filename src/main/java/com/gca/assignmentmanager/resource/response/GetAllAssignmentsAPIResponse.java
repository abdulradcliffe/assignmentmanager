package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gca.assignmentmanager.dto.AssignmentDto;
import com.gca.assignmentmanager.entities.AssignmentEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
public class GetAllAssignmentsAPIResponse {

    private String message;
    private List<AssignmentDto> assignments;
}
