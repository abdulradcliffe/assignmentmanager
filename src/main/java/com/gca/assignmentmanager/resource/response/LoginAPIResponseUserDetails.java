package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gca.assignmentmanager.enums.Role;
import lombok.Data;

import java.util.Date;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginAPIResponseUserDetails {

    private Long id;
    private String name;
    private String email;
    private Date dob;
    private Role role;
    private String className;
}
