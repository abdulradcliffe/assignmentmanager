package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetSubmissionForAssignmentAPIResponse {

    private String status;
    private String message;
    private List<SubmissionMstDto> submissions;
}
