package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gca.assignmentmanager.entities.UserEntity;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GetTeacherClassSubjectAPIResponse {

    private List<GetTeacherClassSubjectAPIResponseData> data;
    private String message;
}
