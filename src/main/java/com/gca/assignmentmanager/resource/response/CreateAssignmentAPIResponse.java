package com.gca.assignmentmanager.resource.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateAssignmentAPIResponse {

    private String status;
    private String message;
    private Long assignmentId;
}
