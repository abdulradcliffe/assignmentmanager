package com.gca.assignmentmanager.resource.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gca.assignmentmanager.entities.SubjectEntity;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GetTeacherClassSubjectAPIResponseData {

    private Long classId;
    private String className;
    private List<SubjectEntity> subjects;
}
