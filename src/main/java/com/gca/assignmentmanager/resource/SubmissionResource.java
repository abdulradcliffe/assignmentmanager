package com.gca.assignmentmanager.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gca.assignmentmanager.entities.*;
import com.gca.assignmentmanager.enums.Constants;
import com.gca.assignmentmanager.repo.*;
import com.gca.assignmentmanager.resource.request.AddSubmissionAPIRequest;
import com.gca.assignmentmanager.resource.request.QADto;
import com.gca.assignmentmanager.resource.request.UpdateUserDetailsAPIRequest;
import com.gca.assignmentmanager.resource.response.*;
import com.gca.assignmentmanager.service.CommunicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping(value = "/assignmentmanager")
public class SubmissionResource {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SubmissionDetailRepository submissionDetailRepository;

    @Autowired
    private SubmissionMasterRepository submissionMasterRepository;

    @Autowired
    private AssignmentRepository assignmentRepository;

    @Autowired
    private AssignmentQuestionRepository assignmentQuestionRepository;


    @Transactional
    @PostMapping("/submissions/add")
    public ResponseEntity addSubmission(@RequestBody AddSubmissionAPIRequest request) throws Exception {
        try {
            validateAddSubmissionAPIRequest(request);
            List<QADto> answers = request.getAnswers();
            Long assignmentId = request.getAssignmentId();
            Long studentId = request.getStudentId();
            SubmissionMasterEntity submissionMaster = new SubmissionMasterEntity();
            submissionMaster.setAssignmentId(assignmentId);
            submissionMaster.setSubmittedBy(studentId);
            submissionMaster.setCreatedOn(new Date());
            submissionMaster = submissionMasterRepository.save(submissionMaster);
            for (QADto dto : answers) {
                SubmissionDetailEntity submissionDetail = new SubmissionDetailEntity();
                submissionDetail.setAnswerText(dto.getAnswerText());
                submissionDetail.setQuestionId(dto.getQuestionId());
                submissionDetail.setSubmissionId(submissionMaster.getId());
                submissionDetailRepository.save(submissionDetail);
            }
            return ResponseEntity.ok(FailedResponse.builder().message("submission added successfully").status(Constants.SUCCESS).build());
        } catch (Exception e) {
            throw e;
        }
    }

    private void validateAddSubmissionAPIRequest(AddSubmissionAPIRequest request) throws Exception {
        if (request.getStudentId() == null) {
            throw new Exception("studentId can not be null");
        } else {
            UserEntity byId = userRepository.findById(request.getStudentId()).orElse(null);
            if (byId == null) {
                throw new Exception("studentId not valid");
            }
        }
        if (request.getAssignmentId() == null) {
            throw new Exception("assignmentId can not be null");
        } else {
            AssignmentEntity byId = assignmentRepository.findById(request.getAssignmentId()).orElse(null);
            if (byId == null) {
                throw new Exception("assignmentId not valid");
            }
        }
        if (request.getAnswers() == null || request.getAnswers().isEmpty()) {
            throw new Exception("answers can not be null");
        } else {
            for (QADto dto : request.getAnswers()) {
                Optional<AssignmentQuestionEntity> byId = assignmentQuestionRepository.findById(dto.getQuestionId());
                if (!byId.isPresent()) {
                    throw new Exception(dto.getQuestionId() + "is not a valid questionId");
                }
            }
        }
    }

    @GetMapping(value = "/submissions")
    public ResponseEntity getSubmissionsForAssignment(@RequestParam(value = "assignmentId", required = false) Long assignmentId) throws Exception {
        if (assignmentId == null) {
            throw new Exception("please provide assignmentId");
        }
        List<SubmissionMasterEntity> submissionEntities = submissionMasterRepository.findByAssignmentId(assignmentId);
        List<SubmissionMstDto> submissions = new LinkedList<>();
        for (SubmissionMasterEntity submissionEntity : submissionEntities) {
            UserEntity userEntity = userRepository.findById(submissionEntity.getSubmittedBy()).orElse(null);
            if (userEntity == null) {
                continue;
            }
            SubmissionMstDto dto = SubmissionMstDto.builder()
                    .submissionDate(submissionEntity.getCreatedOn())
                    .submissionId(submissionEntity.getId())
                    .submittedById(submissionEntity.getSubmittedBy())
                    .submittedByName(userEntity.getName())
                    .build();
            submissions.add(dto);
        }
        GetSubmissionForAssignmentAPIResponse response = GetSubmissionForAssignmentAPIResponse.builder()
                .status(Constants.SUCCESS)
                .message("submissions fetched successfully")
                .submissions(submissions)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/submissions/{submissionId}")
    public ResponseEntity getSubmissionsDetails(@PathVariable(value = "submissionId", required = true) Long submissionId) throws Exception {
        if (submissionId == null) {
            throw new Exception("please provide submissionId");
        }
        SubmissionMasterEntity submissionEntity = submissionMasterRepository.findById(submissionId).orElse(null);
        if (submissionEntity == null) {
            throw new Exception("submissionId is not correct");
        }
        List<SubmissionQuestionAnswerDto> answers = new LinkedList<>();
        List<SubmissionDetailEntity> submissionDetailEntities = submissionDetailRepository.findBySubmissionId(submissionId);
        for (SubmissionDetailEntity submissionDetail : submissionDetailEntities) {
            AssignmentQuestionEntity questionEntity = assignmentQuestionRepository.findById(submissionDetail.getQuestionId()).orElse(null);
            if (questionEntity == null) {
                continue;
            }
            SubmissionQuestionAnswerDto dto = SubmissionQuestionAnswerDto.builder()
                    .answerText(submissionDetail.getAnswerText())
                    .question(questionEntity.getQuestionText())
                    .build();
            answers.add(dto);
        }
        GetSubmissionDetailAPIResponse response = GetSubmissionDetailAPIResponse.builder()
                .status(Constants.SUCCESS)
                .message("submission details fetched successfully")
                .answers(answers)
                .build();
        return ResponseEntity.ok(response);
    }

}
