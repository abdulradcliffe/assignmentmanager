package com.gca.assignmentmanager.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gca.assignmentmanager.dto.AssignmentDto;
import com.gca.assignmentmanager.dto.AssignmentQuestionDto;
import com.gca.assignmentmanager.entities.*;
import com.gca.assignmentmanager.enums.Constants;
import com.gca.assignmentmanager.repo.*;
import com.gca.assignmentmanager.resource.request.CreateAssignmentAPIRequest;
import com.gca.assignmentmanager.resource.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping(value = "/assignmentmanager", produces = MediaType.APPLICATION_JSON_VALUE)
public class AssignmentResource {


    @Autowired
    private AssignmentRepository assignmentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StudentClassMappingRepository studentClassMappingRepository;

    @Autowired
    private AssignmentQuestionRepository assignmentQuestionRepository;

    @Autowired
    private SubmissionMasterRepository submissionMasterRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    public ResponseEntity addAssignment() {
        return ResponseEntity.ok("");
    }

    @Autowired
    private ObjectMapper objectMapper;


    @PostMapping("/createNewAssignment")
    public ResponseEntity createAssignment(@RequestBody CreateAssignmentAPIRequest request) {
        try {
            validate(request);
            AssignmentEntity assignmentEntity = new AssignmentEntity();
            assignmentEntity.setAssignmentName(request.getAssignmentName());
            assignmentEntity.setClassId(request.getClassId());
            assignmentEntity.setCreatedOn(new Date());
            assignmentEntity.setSubjectId(request.getSubjectId());
            assignmentEntity.setTeacherId(request.getTeacherId());
            assignmentEntity.setTopic(request.getAssignmentTopic());
            assignmentEntity = assignmentRepository.save(assignmentEntity);

            List<String> questions = request.getQuestions();
            List<AssignmentQuestionEntity> questionEntities = new LinkedList<>();
            for (String question : questions) {
                AssignmentQuestionEntity assignmentQuestionEntity = new AssignmentQuestionEntity();
                assignmentQuestionEntity.setAssignmentId(assignmentEntity.getId());
                assignmentQuestionEntity.setQuestionText(question);
                questionEntities.add(assignmentQuestionEntity);
            }
            assignmentQuestionRepository.saveAll(questionEntities);
            CreateAssignmentAPIResponse responseBody = CreateAssignmentAPIResponse.builder()
                    .message(Constants.SUCCESS)
                    .status(Constants.SUCCESS)
                    .assignmentId(assignmentEntity.getId())
                    .build();
            return ResponseEntity.ok(responseBody);
        } catch (Exception e) {
            return ResponseEntity.ok(CreateAssignmentAPIResponse.builder().message(e.getMessage()).status(Constants.FAILED).build());
        }
    }

    private void validate(CreateAssignmentAPIRequest request) throws Exception {
        if (StringUtils.isEmpty(request.getAssignmentTopic())) {
            throw new Exception("Assignment Topic not provided");
        }
        if (StringUtils.isEmpty(request.getClassId())) {
            throw new Exception("classId not provided");
        }
        if (StringUtils.isEmpty(request.getSubjectId())) {
            throw new Exception("subjectId not provided");
        }
        if (StringUtils.isEmpty(request.getTeacherId())) {
            throw new Exception("teacherId not provided");
        }
        if (request.getQuestions() == null || request.getQuestions().isEmpty()) {
            throw new Exception("questions not provided");
        }

    }


    @GetMapping("/getAllAssignments")
    public ResponseEntity getAllAssignments(@RequestParam(value = "teacherId", required = false) Long teacherId,
                                            @RequestParam(value = "classId", required = false) Long classId,
                                            @RequestParam(value = "subjectId", required = false) Long subjectId) {
        if (StringUtils.isEmpty(teacherId)) {
            return ResponseEntity.ok(GetAllAssignmentsAPIResponse.builder().message("teacherId Not provided").build());
        }
        List<AssignmentDto> list = new LinkedList<>();
        List<AssignmentEntity> entities = new LinkedList<>();
        try {
            if (StringUtils.isEmpty(classId) && StringUtils.isEmpty(subjectId)) {
                entities = assignmentRepository.findByTeacherId(teacherId);
            } else if (StringUtils.isEmpty(subjectId)) {
                entities = assignmentRepository.findByTeacherIdAndClassId(teacherId, classId);
            } else if (StringUtils.isEmpty(classId)) {
                entities = assignmentRepository.findByTeacherIdAndSubjectId(teacherId, subjectId);
            } else {
                entities = assignmentRepository.findByTeacherIdAndClassIdAndSubjectId(teacherId, classId, subjectId);
            }
            entities.forEach(entity -> {
                AssignmentDto assignmentDto = objectMapper.convertValue(entity, AssignmentDto.class);
                list.add(assignmentDto);
            });
            list.forEach(x -> {
                List<AssignmentQuestionEntity> questionEntities = assignmentQuestionRepository.findByAssignmentId(x.getId());
                List<AssignmentQuestionDto> questions = new LinkedList<>();
                for (AssignmentQuestionEntity questionEntity : questionEntities) {
                    AssignmentQuestionDto assignmentQuestionDto = objectMapper.convertValue(questionEntity, AssignmentQuestionDto.class);
                    questions.add(assignmentQuestionDto);
                }
                x.setQuestions(questions);
            });
            return ResponseEntity.ok(GetAllAssignmentsAPIResponse.builder().message("success").assignments(list).build());
        } catch (Exception e) {
            return ResponseEntity.ok(GetAllAssignmentsAPIResponse.builder().message(e.getMessage()).build());
        }
    }

    @GetMapping("/getAssignment")
    public ResponseEntity getAssignmentDetails(@RequestParam("assignmentId") Long assignmentId) {
        Optional<AssignmentEntity> byId = assignmentRepository.findById(assignmentId);
        if (!byId.isPresent()) {
            return ResponseEntity.ok(FailedResponse.builder().status(Constants.FAILED).message("invalid assignmentId").build());
        }
        AssignmentEntity assignmentEntity = byId.get();
        List<AssignmentQuestionEntity> questionEntities = assignmentQuestionRepository.findByAssignmentId(assignmentEntity.getId());
        List<AssignmentDto> list = new LinkedList<>();
        List<AssignmentQuestionDto> questions = new LinkedList<>();
        for (AssignmentQuestionEntity questionEntity : questionEntities) {
            AssignmentQuestionDto assignmentQuestionDto = objectMapper.convertValue(questionEntity, AssignmentQuestionDto.class);
            questions.add(assignmentQuestionDto);
        }
        AssignmentDto assignmentDto = objectMapper.convertValue(assignmentEntity, AssignmentDto.class);
        assignmentDto.setQuestions(questions);
        return ResponseEntity.ok(GetAssignmentDetailsAPIResponse.builder().message(Constants.SUCCESS).status(Constants.SUCCESS).data(assignmentDto).build());
    }


    @Transactional
    @GetMapping("/deleteAssignment")
    public ResponseEntity deleteAssignment(@RequestParam("assignmentId") Long assignmentId) {
        try {
            Optional<AssignmentEntity> byId = assignmentRepository.findById(assignmentId);
            if (!byId.isPresent()) {
                return ResponseEntity.ok(FailedResponse.builder().status(Constants.SUCCESS).message("assignment already deleted").build());
            }
            AssignmentEntity assignmentEntity = byId.get();
            assignmentQuestionRepository.deleteByAssignmentId(assignmentEntity.getId());
            assignmentRepository.deleteById(assignmentId);
            return ResponseEntity.ok(GetAssignmentDetailsAPIResponse.builder().message(Constants.SUCCESS).status(Constants.SUCCESS).build());
        } catch (Exception e) {
            return ResponseEntity.ok(FailedResponse.builder().status(Constants.FAILED).message(e.getMessage()).build());
        }
    }


    @GetMapping("/getPendingAssignments")
    public ResponseEntity getPendingAssignments(@RequestParam(value = "studentId", required = false) Long studentId) {
        try {
            if (studentId == null) {
                throw new Exception("please provide studentId");
            }
            Optional<UserEntity> user = userRepository.findById(studentId);
            if (!user.isPresent()) {
                throw new Exception("invalid userId. user not found");
            }
            StudentClassMappingEntity mappingEntity = studentClassMappingRepository.findByStudentId(studentId);
            if (mappingEntity == null) {
                throw new Exception("this user doesnot belong to any class");
            }
            Long studentClassId = mappingEntity.getClassId();
            List<AssignmentEntity> assignmentEntities = assignmentRepository.findByClassId(studentClassId);
            List<AssignmentEntity> pendingAssignmentEntities = new LinkedList<>();
            List<GetPendingAssignmentAPIData> data = new LinkedList<>();
            for (AssignmentEntity assignmentEntity : assignmentEntities) {
                SubmissionMasterEntity submissionEntity = submissionMasterRepository.findByAssignmentIdAndSubmittedBy(assignmentEntity.getId(), studentId);
                if (submissionEntity == null) {
                    pendingAssignmentEntities.add(assignmentEntity);
                }
            }
            for (AssignmentEntity assignmentEntity : pendingAssignmentEntities) {
                String subjectName = subjectRepository.findById(assignmentEntity.getSubjectId()).get().getSubjectName();
                String teacherName = userRepository.findById(assignmentEntity.getTeacherId()).get().getName();
                GetPendingAssignmentAPIData dataItem = GetPendingAssignmentAPIData.builder()
                        .assignmentId(assignmentEntity.getId())
                        .createdOn(assignmentEntity.getCreatedOn())
                        .subject(subjectName)
                        .teacherName(teacherName)
                        .topicName(assignmentEntity.getTopic())
                        .build();
                data.add(dataItem);
            }
            return ResponseEntity.ok(GetPendingAssignmentAPIResponse.builder().message(Constants.SUCCESS).status(Constants.SUCCESS).data(data).build());
        } catch (Exception e) {
            return ResponseEntity.ok(FailedResponse.builder().status(Constants.FAILED).message(e.getMessage()).build());
        }
    }


    @GetMapping("/getSubmittedAssignments")
    public ResponseEntity getSubmittedAssignments(@RequestParam(value = "studentId", required = false) Long studentId) {
        try {
            if (studentId == null) {
                throw new Exception("please provide studentId");
            }
            Optional<UserEntity> user = userRepository.findById(studentId);
            if (!user.isPresent()) {
                throw new Exception("invalid userId. user not found");
            }
            StudentClassMappingEntity mappingEntity = studentClassMappingRepository.findByStudentId(studentId);
            if (mappingEntity == null) {
                throw new Exception("this user doesnot belong to any class");
            }
            List<SubmissionMasterEntity> submissionEntities = submissionMasterRepository.findBySubmittedBy(studentId);
            HashMap<Long, SubmissionMasterEntity> assignmentIdVsSubmissionEntity = new HashMap<>();
            for (SubmissionMasterEntity entity : submissionEntities) {
                assignmentIdVsSubmissionEntity.put(entity.getAssignmentId(), entity);
            }
            List<Long> submittedAssignmentIds = submissionEntities.stream().map(SubmissionMasterEntity::getAssignmentId).collect(Collectors.toList());
            List<AssignmentEntity> submittedAssignmentEntities = assignmentRepository.findByIdIn(submittedAssignmentIds);
            List<GetSubmittedAssignmentAPIData> data = new LinkedList<>();
            for (AssignmentEntity assignmentEntity : submittedAssignmentEntities) {
                String subjectName = subjectRepository.findById(assignmentEntity.getSubjectId()).get().getSubjectName();
                String teacherName = userRepository.findById(assignmentEntity.getTeacherId()).get().getName();
                GetSubmittedAssignmentAPIData dataItem = GetSubmittedAssignmentAPIData.builder()
                        .assignmentId(assignmentEntity.getId())
                        .createdOn(assignmentEntity.getCreatedOn())
                        .subject(subjectName)
                        .teacherName(teacherName)
                        .topicName(assignmentEntity.getTopic())
                        .submissionId(assignmentIdVsSubmissionEntity.get(assignmentEntity.getId()).getId())
                        .submissionDate(assignmentIdVsSubmissionEntity.get(assignmentEntity.getId()).getCreatedOn())
                        .build();
                data.add(dataItem);
            }
            return ResponseEntity.ok(GetSubmittedAssignmentAPIResponse.builder().message(Constants.SUCCESS).status(Constants.SUCCESS).data(data).build());
        } catch (Exception e) {
            return ResponseEntity.ok(FailedResponse.builder().status(Constants.FAILED).message(e.getMessage()).build());
        }
    }
}

