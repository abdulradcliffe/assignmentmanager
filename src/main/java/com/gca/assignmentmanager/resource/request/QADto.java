package com.gca.assignmentmanager.resource.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QADto {

    private Long questionId;
    private String answerText;
}
