package com.gca.assignmentmanager.resource.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddSubmissionAPIRequest {

    private Long studentId;
    private Long assignmentId;
    private List<QADto> answers;
}
