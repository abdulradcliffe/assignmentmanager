package com.gca.assignmentmanager.resource.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateAssignmentAPIRequest {

    private Long teacherId;
    private Long classId;
    private Long subjectId;
    private String assignmentName;
    private String assignmentTopic;
    private List<String> questions;
}
