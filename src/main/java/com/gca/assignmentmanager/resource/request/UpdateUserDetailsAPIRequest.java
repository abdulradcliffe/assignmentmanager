package com.gca.assignmentmanager.resource.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateUserDetailsAPIRequest {

    private Long userId;
    private String name;
    private String email;
    private String dob;
    private String password;
}
