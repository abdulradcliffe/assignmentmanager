package com.gca.assignmentmanager.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gca.assignmentmanager.entities.ClassEntity;
import com.gca.assignmentmanager.entities.StudentClassMappingEntity;
import com.gca.assignmentmanager.entities.UserEntity;
import com.gca.assignmentmanager.enums.Role;
import com.gca.assignmentmanager.repo.ClassRepository;
import com.gca.assignmentmanager.repo.StudentClassMappingRepository;
import com.gca.assignmentmanager.repo.UserRepository;
import com.gca.assignmentmanager.resource.response.LoginAPIResponse;
import com.gca.assignmentmanager.resource.response.LoginAPIResponseUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping(value = "/assignmentmanager")
public class LoginResource {


    @Autowired
    private UserRepository userRepository;

    private static Logger logger = LoggerFactory.getLogger(LoginResource.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private StudentClassMappingRepository studentClassMappingRepository;

    @Autowired
    private ClassRepository classRepository;

    @GetMapping("/login")
    public ResponseEntity login(@RequestParam(value = "email", required = false) String email, @RequestParam(value = "password", required = false) String password) {
        LoginAPIResponse response = new LoginAPIResponse();
        logger.info("login API Called: email: {}, password: {}", email, password);
        if (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)) {
            response.setStatus(false);
            response.setMessage("email/password not provided");
            return ResponseEntity.ok(response);
        }

        UserEntity userEntity = userRepository.findByEmailAndPassword(email, password);
        if (userEntity == null) {
            response.setStatus(false);
            response.setMessage("invalid email and password");
            return ResponseEntity.ok(response);
        } else {
            logger.info("login success. user found: {}", userEntity);
            response.setStatus(true);
            response.setMessage("login succeed");
            LoginAPIResponseUserDetails userDetails = objectMapper.convertValue(userEntity, LoginAPIResponseUserDetails.class);
            response.setUserDetail(userDetails);
            if (Role.STUDENT == userDetails.getRole()) {
                logger.info("user role is student");
                StudentClassMappingEntity mappingEntity = studentClassMappingRepository.findByStudentId(userDetails.getId());
                ClassEntity classEntity = classRepository.findById(mappingEntity.getClassId()).get();
                logger.info("user class: {}", classEntity.getClassName());
                userDetails.setClassName(classEntity.getClassName());
            }
            return ResponseEntity.ok(response);
        }
    }

}
