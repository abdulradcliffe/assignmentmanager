package com.gca.assignmentmanager.resource;

import com.gca.assignmentmanager.entities.ClassEntity;
import com.gca.assignmentmanager.entities.SubjectEntity;
import com.gca.assignmentmanager.entities.TeacherClassSubjectmappingEntity;
import com.gca.assignmentmanager.entities.UserEntity;
import com.gca.assignmentmanager.repo.ClassRepository;
import com.gca.assignmentmanager.repo.SubjectRepository;
import com.gca.assignmentmanager.repo.TeacherClassSubjectMappingRepository;
import com.gca.assignmentmanager.repo.UserRepository;
import com.gca.assignmentmanager.resource.response.GetTeacherClassSubjectAPIResponse;
import com.gca.assignmentmanager.resource.response.GetTeacherClassSubjectAPIResponseData;
import com.gca.assignmentmanager.resource.response.LoginAPIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/assignmentmanager", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClassSubjectResource {


    @Autowired
    private TeacherClassSubjectMappingRepository mappingRepository;

    @Autowired
    private ClassRepository classRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @GetMapping("/getClassSubjectForTeacher")
    public ResponseEntity getClassSubjectForTeacher(@RequestParam(value = "teacherId", required = false) Long teacherId) {
        GetTeacherClassSubjectAPIResponse response = new GetTeacherClassSubjectAPIResponse();
        if (StringUtils.isEmpty(teacherId)) {
            response.setMessage("teacherId not provided");
            return ResponseEntity.ok(response);
        }
        List<GetTeacherClassSubjectAPIResponseData> data = new LinkedList<>();
        List<TeacherClassSubjectmappingEntity> mappings = mappingRepository.findByTeacherId(teacherId);
        Map<Long, List<Long>> classIdVsSubjectIds = new HashMap<>();
        for (TeacherClassSubjectmappingEntity entity : mappings) {
            if (classIdVsSubjectIds.containsKey(entity.getClassId())) {
                List<Long> subjectIds = classIdVsSubjectIds.get(entity.getClassId());
                subjectIds.add(entity.getSubjectId());
            } else {
                List<Long> subjectIds = new ArrayList<>();
                subjectIds.add(entity.getSubjectId());
                classIdVsSubjectIds.put(entity.getClassId(), subjectIds);
            }
        }

        Set<Map.Entry<Long, List<Long>>> entries = classIdVsSubjectIds.entrySet();
        for (Map.Entry<Long, List<Long>> entry : entries) {
            Long classId = entry.getKey();
            List<Long> subjectIds = entry.getValue();
            GetTeacherClassSubjectAPIResponseData object = new GetTeacherClassSubjectAPIResponseData();
            ClassEntity classEntity = classRepository.findById(classId).get();
            object.setClassId(classEntity.getId());
            object.setClassName(classEntity.getClassName());
            List<SubjectEntity> subjectEntities = subjectRepository.findByIdIn(subjectIds);
            object.setSubjects(subjectEntities);
            data.add(object);
        }
        response.setData(data);
        return ResponseEntity.ok(response);
    }

}
