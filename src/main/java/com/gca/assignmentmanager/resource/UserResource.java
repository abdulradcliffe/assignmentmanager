package com.gca.assignmentmanager.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gca.assignmentmanager.entities.UserEntity;
import com.gca.assignmentmanager.enums.Constants;
import com.gca.assignmentmanager.repo.UserRepository;
import com.gca.assignmentmanager.resource.request.UpdateUserDetailsAPIRequest;
import com.gca.assignmentmanager.resource.response.FailedResponse;
import com.gca.assignmentmanager.resource.response.GetUserDetailAPIResponse;
import com.gca.assignmentmanager.resource.response.LoginAPIResponse;
import com.gca.assignmentmanager.resource.response.UserDto;
import com.gca.assignmentmanager.service.CommunicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@CrossOrigin
@RestController
@RequestMapping(value = "/assignmentmanager")
public class UserResource {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CommunicationService communicationService;

    @GetMapping("/users/{userId}")
    public ResponseEntity getUserDetails(@PathVariable(value = "userId", required = false) Long userId) {
        if (StringUtils.isEmpty(userId)) {
            return ResponseEntity.ok(FailedResponse.builder().message("Please provide userId").build());
        }
        UserEntity userEntity = userRepository.findById(userId).orElse(null);
        if (userEntity != null) {
            GetUserDetailAPIResponse response = new GetUserDetailAPIResponse();
            response.setStatus(true);
            response.setMessage("success");
            UserDto userDto = objectMapper.convertValue(userEntity, UserDto.class);
            response.setData(userDto);
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.ok(FailedResponse.builder().message("no user found with userId: " + userId).build());
        }
    }


    @Transactional
    @PostMapping("/users/updateUserDetails")
    public ResponseEntity updateUserDetails(@RequestBody UpdateUserDetailsAPIRequest request) {
        if (StringUtils.isEmpty(request.getUserId())) {
            return ResponseEntity.ok(FailedResponse.builder().message("Please provide userId").build());
        }
        UserEntity userEntity = userRepository.findById(request.getUserId()).orElse(null);
        if (userEntity != null) {
            if (!StringUtils.isEmpty(request.getDob())) {
                try {
                    Date parse = new SimpleDateFormat("yyyy-MM-dd").parse(request.getDob());
                    userEntity.setDob(parse);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (!StringUtils.isEmpty(request.getEmail())) {
                userEntity.setEmail(request.getEmail());
            }
            if (!StringUtils.isEmpty(request.getName())) {
                userEntity.setName(request.getName());
            }
            if (!StringUtils.isEmpty(request.getPassword())) {
                userEntity.setPassword(request.getPassword());
            }
            userEntity = userRepository.save(userEntity);
            GetUserDetailAPIResponse response = new GetUserDetailAPIResponse();
            response.setStatus(true);
            response.setMessage("updated successfully");
            UserDto userDto = objectMapper.convertValue(userEntity, UserDto.class);
            response.setData(userDto);
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.ok(FailedResponse.builder().message("no user found with userId: " + request.getUserId()).build());
        }
    }


    @Transactional
    @GetMapping("/users/resetpassword")
    public ResponseEntity resetPassword(@RequestParam(value = "email", required = false) String email) throws IOException, MessagingException {
        if (StringUtils.isEmpty(email)) {
            return ResponseEntity.ok(FailedResponse.builder().message("Please provide emailId").build());
        }
        try {
            UserEntity userEntity = userRepository.findByEmail(email);
            if (userEntity == null) {
                return ResponseEntity.ok(FailedResponse.builder().message("no user found with the given emailId").build());
            }
            String content = "Please use the following as your password to login into the System:  <b>" + userEntity.getPassword() + "</b>";
            communicationService.sendMail(userEntity.getEmail(),content);
            return ResponseEntity.ok(FailedResponse.builder().status(Constants.SUCCESS).message("success").build());
        } catch (Exception e) {
            throw e;
        }
    }

}
