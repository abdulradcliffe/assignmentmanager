package com.gca.assignmentmanager.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

@Service
public class CommunicationService {

    private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    private static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
    private static final String MAIL_SMTP_HOST = "mail.smtp.host";
    private static final String MAIL_SMTP_PORT = "mail.smtp.port";
    private static final String TRUE = "true";
    private static final String SMTP_GMAIL_COM = "smtp.gmail.com";
    private static final String PORT_VALUE = "587";

    @Value("${gmail.password}")
    private String gmailPassword;

    @Value("${gmail.id}")
    private String gmailId;


    public void sendMail(String toEmailAddress, String mailContent) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put(MAIL_SMTP_AUTH, TRUE);
        props.put(MAIL_SMTP_STARTTLS_ENABLE, TRUE);
        props.put(MAIL_SMTP_HOST, SMTP_GMAIL_COM);
        props.put(MAIL_SMTP_PORT, PORT_VALUE);

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(gmailId, gmailPassword);
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(gmailId, false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmailAddress));
        msg.setSubject("Gilbert Classical Academy");
        msg.setContent(mailContent, "text/html");
        msg.setSentDate(new Date());
        Transport.send(msg);
    }

}
