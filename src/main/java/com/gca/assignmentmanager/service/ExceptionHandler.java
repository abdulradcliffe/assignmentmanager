package com.gca.assignmentmanager.service;

import com.gca.assignmentmanager.enums.Constants;
import com.gca.assignmentmanager.resource.response.FailedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception e) {
        logger.error("handleException: ", e);
        FailedResponse build = FailedResponse.builder().message(e.getMessage()).status(Constants.FAILED).build();
        return new ResponseEntity<>(build, HttpStatus.OK);
    }

}
